﻿Imports System.Globalization
Imports MaterialSkin
Imports MaterialSkin.Controls

Public Class Form1
    Dim top As Integer = 10
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim SkinManager As MaterialSkinManager = MaterialSkinManager.Instance
        SkinManager.AddFormToManage(Me)
        SkinManager.Theme = MaterialSkinManager.Themes.LIGHT
        SkinManager.ColorScheme = New ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE)

    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles AddTodo.Click
        ListBox1.Items.Add(todo.Text + "  | " + DateTime.Today.Date.
                           ToString("dd/MMM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture))
    End Sub
    Private Sub MaterialRaisedButton1_Click_1(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        ListBox1.Items.Remove(ListBox1.SelectedItem)
    End Sub
End Class
