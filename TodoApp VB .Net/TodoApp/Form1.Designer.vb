﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    'Inherits System.Windows.Forms.Form
    Inherits MaterialSkin.Controls.MaterialForm
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.AddTodo = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.todo = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.SuspendLayout()
        '
        'AddTodo
        '
        Me.AddTodo.Depth = 0
        Me.AddTodo.Location = New System.Drawing.Point(250, 83)
        Me.AddTodo.MouseState = MaterialSkin.MouseState.HOVER
        Me.AddTodo.Name = "AddTodo"
        Me.AddTodo.Primary = True
        Me.AddTodo.Size = New System.Drawing.Size(57, 23)
        Me.AddTodo.TabIndex = 1
        Me.AddTodo.Text = "+ Add"
        Me.AddTodo.UseVisualStyleBackColor = True
        '
        'todo
        '
        Me.todo.BackColor = System.Drawing.SystemColors.Control
        Me.todo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.todo.Depth = 0
        Me.todo.Hint = ""
        Me.todo.Location = New System.Drawing.Point(12, 83)
        Me.todo.MouseState = MaterialSkin.MouseState.HOVER
        Me.todo.Name = "todo"
        Me.todo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.todo.SelectedText = ""
        Me.todo.SelectionLength = 0
        Me.todo.SelectionStart = 0
        Me.todo.Size = New System.Drawing.Size(215, 23)
        Me.todo.TabIndex = 2
        Me.todo.Text = "..."
        Me.todo.UseSystemPasswordChar = False
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 24
        Me.ListBox1.Location = New System.Drawing.Point(30, 125)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(362, 292)
        Me.ListBox1.TabIndex = 3
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(313, 82)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(73, 23)
        Me.MaterialRaisedButton1.TabIndex = 4
        Me.MaterialRaisedButton1.Text = "- Delete"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(398, 450)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.todo)
        Me.Controls.Add(Me.AddTodo)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "TodoApp"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents AddTodo As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents todo As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
End Class
